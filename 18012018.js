// task1
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'mongo_practice';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);
  const collection = db.collection('movies');
  const users = db.collection('users');
  const posts = db.collection('posts');
  const comments = db.collection('comments');
//   collection.insertMany(
//       [
//     {
//         title : "Fight Club",
//         writer : "Chuck Palahniuk",
//         year : 1999,
//         actors : [
//           "Brad Pitt",
//           "Edward Norton"
//         ]
//     },
//     {
//         title : "Pulp Fiction",
//         writer : "Quentin Tarantino",
//         year : 1994,
//         actors : [
//         "John Travolta",
//         "Uma Thurman"
//         ]
//     },
//     {
//         title : "Inglorious Basterds",
//         writer : "Quentin Tarantino",
//         year : 2009,
//         actors : [
//         "Brad Pitt",
//         "Diane Kruger",
//         "Eli Roth"
//         ]
//     },
//     {
//         title : "The Hobbit: An Unexpected Journey",
//         writer : "J.R.R. Tolkein",
//         year : 2012,
//         franchise : "The Hobbit"
//     },
//     {
//         title : "The Hobbit: The Desolation of Smaug",
//         writer : "J.R.R. Tolkein",
//         year : 2013,
//         franchise : "The Hobbit"
//     },
//     {
//         title : "The Hobbit: The Battle of the Five Armies",
//         writer : "J.R.R. Tolkein",
//         year : 2012,
//         franchise : "The Hobbit",
//         synopsis : "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."
//     },
//     {
//         title : "Pee Wee Herman's Big Adventure"
//     },
//     {
//         title : "Avatar"
//     }
//       ]
//   );
// var rrrr = collection.find({}).project({title:1}).toArray();
//Query / Find Documents
// 1. get all documents
// var rrrr = collection.find({}).toArray();
// 2. get all documents with writer set to "Quentin Tarantino"
// var rrrr = collection.find({writer:"Quentin Tarantino"}).toArray();
// 3. get all documents where actors include "Brad Pitt"
// var rrrr = collection.find({actors:"Brad Pitt"}).toArray();
// 4. get all documents with franchise set to "The Hobbit"
// var rrrr = collection.find({franchise:"The Hobbit"}).toArray();
// 5. get all movies released in the 90s
// var rrrr = collection.find({year:{$gte:1990, $lt:2000}}).toArray();
// 6. get all movies released before the year 2000 or after 2010
// var rrrr = collection.find({$or:[{year:{$lt:2000}}, {year:{$gt:2010}}]}).toArray();
//Update Documents
// 1. add a synopsis to "The Hobbit: An Unexpected Journey" : "A reluctant hobbit, Bilbo 
// Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their
// mountain home - and the gold within it - from the dragon Smaug."
// var rrrr = collection.update({title:"The Hobbit: An Unexpected Journey"}, {$set:{synopsis:"A 
// reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves
// to reclaim their mountain home - and the gold within it - from the dragon Smaug."}});
// 2. add a synopsis to "The Hobbit: The Desolation of Smaug" : "The dwarves, along with Bilbo 
// Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug.
// Bilbo Baggins is in possession of a mysterious and magical ring."
// var rrrr = collection.update({title:"The Hobbit: The Desolation of Smaug"}, {$set:{synopsis:"The dwarves,
// along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland,
// from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."}});
// 3. add an actor named "Samuel L. Jackson" to the movie "Pulp Fiction"
// var rrrr = collection.update({title:"Pulp Fiction"}, {$push:{actors:"Samuel L. Jackson"}});
//Text Search
// 1. find all movies that have a synopsis that contains the word "Bilbo"
// var rrrr = collection.find({synopsis:{$regex:/.*Bilbo.*/}}).toArray();
// 2. find all movies that have a synopsis that contains the word "Gandalf"
// var rrrr = collection.find({synopsis:{$regex:/.*Gandalf.*/}}).toArray();
// 3. find all movies that have a synopsis that contains the word "Bilbo" and not the word "Gandalf"
// var rrrr = collection.find({$and:[{synopsis:{$regex:/.*Bilbo.*/}}, {synopsis: {$ne:{$regex:'.*Gandalf.*'}}}]}).toArray();
// 4. find all movies that have a synopsis that contains the word "dwarves" or "hobbit"
// var rrrr = collection.find({$or:[{synopsis:{$regex:/.*dwarves.*/}}, {synopsis: {$regex:'.*hobbit.*'}}]}).toArray();
// 5. find all movies that have a synopsis that contains the word "gold" and "dragon"
// var rrrr = collection.find({$and:[{synopsis:{$regex:/.*dragon.*/}}, {synopsis: {$regex:'.*gold.*'}}]}).toArray();
//Delete Documents
// 1. delete the movie "Pee Wee Herman's Big Adventure"
// var rrrr = collection.update({title:"Pee Wee Herman's Big Adventure"}, {$unset:{title:""}});
// var rrrr = collection.deleteOne({title: "Pee Wee Herman's Big Adventure"});
// 2. delete the movie "Avatar"
// var rrrr = collection.deleteOne({title: "Avatar"});
// rrrr.then(result => {console.log(result)});
// Relationships
// users.insertMany(
//     [
//         {
//             username : "GoodGuyGreg",
//             first_name : "Good Guy",
//             last_name : "Greg"
//         },
//         {
//             username : "ScumbagSteve",
//             full_name :
//                 {
//                     first : "Scumbag",
//                     last : "Steve"
//                 }
//         }
//     ]
// );
// posts.insertMany(
//     [
//         {
//             username : "GoodGuyGreg",
//             title : "Passes out at party",
//             body : "Wakes up early and cleans house"
//         },
//         {
//             username : "GoodGuyGreg",
//             title : "Steals your identity",
//             body : "Raises your credit score"
//         },
//         {
//             username : "GoodGuyGreg",
//             title : "Reports a bug in your code",
//             body : "Sends you a Pull Request"
//         },
//         {
//             username : "ScumbagSteve",
//             title : "Borrows something",
//             body : "Sells it"
//         },
//         {
//             username : "ScumbagSteve",
//             title : "Borrows everything",
//             body : "The end"
//         },
//         {
//             username : "ScumbagSteve",
//             title : "Forks your repo on github",
//             body : "Sets to private"
//         }
//     ]
// // );
foo(posts, comments).then(w => {
//WARNING
//server pool is destroyed
//this is nessesary because function wating while promise are finish, but the main stream of code was executed and connection was closed
    client.close();
});
//   client.close();
});


async function foo(pos, col){
    var y1 = await pos.find({title:"Borrows something"}).project({_id:1}).next();
    var y2 = await pos.find({title:"Borrows everything"}).project({_id:1}).next();
    var y3 = await pos.find({title:"Forks your repo on github"}).project({_id:1}).next();
    var y4 = await pos.find({title:"Passes out at party"}).project({_id:1}).next();
    var y5 = await pos.find({title:"Reports a bug in your code"}).project({_id:1}).next();
        await col.insertMany(
            [
                {
            username : "GoodGuyGreg",
            comment : "Hope you got a good deal!",
            post : y1._id
        },
        {
            username : "GoodGuyGreg",
            comment : "Hope you got a good deal!",
            post : y2._id
        },
        {
            username : "GoodGuyGreg",
            comment : "What's mine is yours!",
            post : y3._id
        },
        {
            username : "GoodGuyGreg",
            comment : "Hope you got a good deal!",
            post : y4._id
        },
        {
            username : "GoodGuyGreg",
            comment : "What's mine is yours!",
            post : y5._id
        }
            ]
);
};

